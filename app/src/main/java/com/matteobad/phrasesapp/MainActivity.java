package com.matteobad.phrasesapp;

import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    MediaPlayer player = null;

    public void playPhrase(View view) {
        String tag = view.getTag().toString();
        Log.i("tag", tag);
        int resId = getResources().getIdentifier(tag, "raw", getPackageName());
        Log.i("id", "" + resId);

        try {
            player = MediaPlayer.create(this, resId);
            player.start();

        } catch (Exception e) {
            Log.i("info", e.getMessage());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
